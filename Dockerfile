#FROM python:3.9
#Image to build MKDocs
FROM squidfunk/mkdocs-material
#RUN pip install mkdocs-material mkdocs-git-revision-date-localized-plugin
WORKDIR /docs
COPY . /docs
EXPOSE 8000
#CMD ["mkdocs","serve", "--dev-addr=0.0.0.0:8000"]
#This is to lunch MKdocs
CMD ["serve", "--dev-addr=0.0.0.0:8000"]