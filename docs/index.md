# Welcome to my project
Welcome to the official documentation of my project, you'll find all the tech documentation here.

## Feature
 - Easy to use
 - Highly customizable
 - Build with latest tech

## Installation
```bash
pip install myproject
```

## Usage
```python
import myproject
myproject.hello()
```