provider "aws" {
  region = "us-east-1"
  access_key = var.access_key
  secret_key = var.secret_key
}



terraform {
  backend "s3" {
    bucket = "gitlabcicd-834277767436-tf-state-test"
    key    = "ecr.tfstate"
    region = "us-east-1"
  }
}
# create aws ecr repository

resource "aws_ecr_repository" "repository" {
  name                 = "gitlab-ecr-tutorial"
  image_tag_mutability = "MUTABLE"

  tags = {
    Name      = "gitlab-ecr-tutorial",
    createdby = "terraform"
  }
  image_scanning_configuration {
    scan_on_push = true
  }
}


resource "aws_ecr_repository_policy" "public_policy" {
  repository = aws_ecr_repository.repository.name

  policy = jsonencode({
    Version = "2008-10-17"
    Statement = [
      {
        Sid       = "AllowPublicPull"
        Effect    = "Allow"
        Principal = "*"
        Action = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:GetRepositoryPolicy",
        ]
      }
    ]
  })
}

output "repository_url" {
  value = aws_ecr_repository.repository.repository_url
}
